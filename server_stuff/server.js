const express = require('express');
const cors = require('cors');
const fs = require('fs');
const path = require('path');
const app = express();
const port = 5001; // Updated to use port 5001

app.use(express.json());
app.use(cors());

// Define an endpoint to list files in a directory
app.get('/listFiles', (req, res) => {
  // console.log(`/listFiles/`);
  if (Object.keys(req.query).length === 0) {
    // No query parameters were provided
    const directoryPath = path.join("/Users/martinkoucky/Downloads/tmp/new_file_struct/"); // Replace with the actual directory path
    const files = fs.readdirSync(directoryPath);
    res.json({ files });

  }
  else {
    const subfolder = req.query.pathString;
    // const subfolder = req.params.subfolder;
    const subfolderPath = path.join("/Users/martinkoucky/Downloads/tmp/new_file_struct/", subfolder); // Adjust the path accordingly
    console.log(`fetching: ${subfolderPath}`);
    try {
      const files = fs.readdirSync(subfolderPath);
      res.json({ files });
    } catch (error) {
      console.error('Error:', error);
      res.status(500).json({ error: 'Unable to fetch subfolder contents' });
    }
  }
});

// Define a new endpoint for subfolder contents
// app.get('/listFiles/:subfolder', (req, res) => {
//   // console.log(`/listFiles/:subfolder | should not call this`);
//   const subfolder = req.params.subfolder;
//   const subfolderPath = path.join("/Users/martinkoucky/Downloads/tmp/new_file_struct/", subfolder); // Adjust the path accordingly
//   console.log(`fetching: ${subfolderPath}`);
//   try {
//     const files = fs.readdirSync(subfolderPath);
//     res.json({ files });
//   } catch (error) {
//     console.error('Error:', error);
//     res.status(500).json({ error: 'Unable to fetch subfolder contents' });
//   }
// });

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
