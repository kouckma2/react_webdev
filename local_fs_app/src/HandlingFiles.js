import React, {useEffect, useState} from 'react';
import axios from 'axios';
import FileTree from './FileTree';
// import FileExplorerModal from './FileExplorerModal';
import './ModalStyles.css';
import FileExplorerModal from "./FileExplorerModal";
import { Button } from '@mantine/core';

class JSONFileSaver extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      jsonData: {
        // Your JSON data here
        "key": "value"
      }
    };
  }

  handleSaveJSON = () => {
    const { jsonData } = this.state;
    const jsonBlob = new Blob([JSON.stringify(jsonData, null, 2)], { type: 'application/json' });
    const url = URL.createObjectURL(jsonBlob);

    // Create a temporary <a> element to trigger the download
    const link = document.createElement('a');
    link.href = url;
    link.download = 'data.json';
    document.body.appendChild(link);
    link.click();

    // Cleanup
    URL.revokeObjectURL(url);
    document.body.removeChild(link);
  }

  render() {
    return (
      <div>
        <button onClick={this.handleSaveJSON}>Save JSON</button>
      </div>
    );
  }
}

export default JSONFileSaver;
