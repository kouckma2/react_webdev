import VTComponent from "./VTComponent";

import { MantineProvider } from '@mantine/core';

const App = () => {
  return (
    <MantineProvider withGlobalStyles withNormalizeCSS>
      <VTComponent/>
    </MantineProvider>
  );
};

export default App;
