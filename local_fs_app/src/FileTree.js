import React, { useState } from 'react';
import { BsFolder, BsFolderFill, BsFileEarmark} from 'react-icons/bs';
import './FileTree.css';
import FileExplorerModal from "./FileExplorerModal";
import { Button } from '@mantine/core';


// const Checkbox = ({ label, value }) => {
//   return (
//     <label>
//       <input type="checkbox" checked={value} />
//       {label}
//     </label>
//   );
// };


const FileTree = ({ directoryStructure, fetchSubfolderContents, handleCheck, change_config_path, fetchAllSubfolderContents}) => {

  const renderFileTree = (items) => {
      console.log(`rendering file tree`);

      if (!items) {
      return null;
    }

      return Object.keys(items).map((item) => (
          <div key={item}>
              <div className="tree-item">
                  {items[item].isSelected ? (
                  <input type="checkbox" checked={true} onChange={() => handleCheck(items[item])}/>
                  // <Checkbox label="" value={true} onChange={() => handleCheck(items[item])}/>
                          ) : (
                  <input type="checkbox" checked={false} onChange={() => handleCheck(items[item])}/>
                  // <Checkbox label="" value={false} onChange={() => handleCheck(items[item])} />
                    )}
                  <div className="folder-actions" onClick={() => fetchSubfolderContents(items[item], item, items[item].path)}>
                      {items[item].isOpen ? (
                          <BsFolderFill className="folder-icon"/>
                        ) : (
                          <BsFolder className="folder-icon"/>
                        )
                      }
                      <span>{item}</span>
                  </div>
                  <div>
                      {items[item].depth === 4 &&
                          // <button>{items[item].config_path}</button>
                          <button onClick={() => change_config_path(items[item], items[item].configs_path)}>{items[item].config_path}</button>
                      }
                      {(items[item].depth === 0 || items[item].depth === 1) &&
                          // <button>{items[item].config_path}</button>
                          <button onClick={() => fetchAllSubfolderContents(items[item], item, items[item].path, items[item].depth)}>auto expand</button>
                      }
                  </div>
            </div>
            {items[item].isOpen ? (
              <div className="nested-tree">
                {renderFileTree(items[item].children)}
              </div>
            ) : null}
        </div>
    ));
  };

  return (
    <div>
      <h2 className="tree-heading">Directory Structure</h2>
      {renderFileTree(directoryStructure)}
    </div>
      //       <button onClick={openModal}>Open Modal</button>
      // {isModalOpen && (
      //   <FileExplorerModal openModal={openModal} closeModal={closeModal} />
      // )}

  );

};

export default FileTree;
