import React, {useEffect, useState} from 'react';
import axios from 'axios';
import FileTree from './FileTree';
// import FileExplorerModal from './FileExplorerModal';
import './ModalStyles.css';
import FileExplorerModal from "./FileExplorerModal";
import { Button } from '@mantine/core';

const deepIterate = (obj, parentPath = '', result = []) => {
  for (const key in obj) {
    if (obj.hasOwnProperty(key) && key !== "") {
      const { path, children, config_path} = obj[key];

      // Store the "path" property in the result array

        console.log('obj struct:', JSON.stringify(obj[key], null, 2));
        console.log(path, config_path)
      if (obj[key].isSelected === true) {
          console.log(path, "is selected")
          result.push([path, config_path]);
      }
      if (children && typeof children === 'object') {
        // Recursive call for the "children" property
        deepIterate(children, path, result);
      }
    }
  }

  return result;
};

const makeJson = (directoryStructure) => {
    console.log(`clicked button!!!`);
    const resultArray = deepIterate(directoryStructure[""].children);
    // const resultArray = deepIterate(directoryStructure);
    console.log(resultArray);

    const jsonBlob = new Blob([JSON.stringify(resultArray, null, 2)], { type: 'application/json' });
    // Create a temporary URL for the Blob
    const url = URL.createObjectURL(jsonBlob);

    // Create a temporary <a> element to trigger the download
    const link = document.createElement('a');
    link.href = url;
    link.download = 'data.json';
    document.body.appendChild(link);
    link.click();

    // Cleanup
    URL.revokeObjectURL(url);
    document.body.removeChild(link);
    };

const VTComponent = () => {
    console.log(`running VTComponent.js`);


    const [isModalOpen, setIsModalOpen] = useState(false);
    const [possibleConfigs, setPossibleConfigs] = useState({});
    const [directoryStructure, setDirectoryStructure] = useState({});

    const openModal = (possible_configs) => {
        setPossibleConfigs(possible_configs)
        setIsModalOpen(true);
    };

    const closeModal = () => {
        setIsModalOpen(false);
    };


    const change_config_path = (current_folder, configs_path) => {
        console.log(`changing config path!:`)
        // let configs_path = ""
        // const shortenedString = pathString.substring(0, pathString.length - "/videos".length);
        // configs_path = shortenedString+"/configs/"
        console.log(`configs_path: ${configs_path}`)
        axios.get('http://localhost:5001/listFiles', {params: { pathString: configs_path }})
            .then((response) => {
                let possible_configs = response.data.files.reduce((acc, file) => {
                      acc[file] = {
                          path: configs_path + "/" + file,
                      };
                  return acc;
                }, {})
                openModal(possible_configs)
            })
            .catch((error) => {
              console.error('Error:', error);
            });

        };

    const handleCheck = (current_folder) => {
        console.log(`isSelected previous:`, current_folder.isSelected)
        current_folder.isSelected = !current_folder.isSelected;
        // just react things :)
        const updatedDirectoryStructure = { ...directoryStructure };
        setDirectoryStructure(updatedDirectoryStructure);
    };
    const fetchAllSubfolderContents = (current_folder, folderName, pathString, given_depth) => {
        // THIS IS A RECURSIVE VERSION OF fetchSubfolderContents

        if ((given_depth < 3 || (folderName === "videos")) && !folderName.startsWith(".")) {
            let is_videos = false
            if (folderName === "videos"){
                is_videos = true
            }

            let depth = current_folder.depth

            current_folder.isOpen = true
            let selectedValue = is_videos
            let tmp_children = null

            console.log(`fetching for: ${pathString}`)
            axios.get('http://localhost:5001/listFiles', {params: { pathString: pathString }})
            .then((response) => {
                current_folder.children = response.data.files.reduce((acc, file) => {
                    if (!file.startsWith(".")){
                        const regex = /(\d{4}-\d{2}-\d{2}-\d{4})/;
                        const match = regex.exec(file);
                        const dateTimeString = match && match[1];
                        let configs_path = ""
                        if (current_folder.depth === 3) {
                            const shortenedString = pathString.substring(0, pathString.length - "/videos".length);
                            configs_path = shortenedString+"/config"
                            console.log(`configs_path: ${configs_path} ${pathString}`)
                        }
                      acc[file] = {
                          isDirectory: true,
                          path: pathString + "/" + file,
                          config_path: configs_path + "/" + dateTimeString + ".yaml",
                          configs_path: configs_path,
                          isOpen: false,
                          isSelected: selectedValue,
                          children: null,
                          depth: depth+1
                      };
                    }
                  return acc;
                }, {})
                tmp_children = current_folder.children
                current_folder.isOpen = true

                // just react things :)
                const updatedDirectoryStructure = { ...directoryStructure };
                setDirectoryStructure(updatedDirectoryStructure);

                for (const key in current_folder.children) {
                    fetchAllSubfolderContents(current_folder.children[key], key, current_folder.children[key].path, given_depth+1)
                }

            })
            .catch((error) => {
              console.error('Error:', error);
            });
        }
    };
    const fetchSubfolderContents = (current_folder, folderName, pathString) => {
        const pathArray = pathString.split('/');
        console.log(pathArray);
        // const pathString = 'folder1/folder2/folder3/file.txt';
        // const pathArray = pathString.split('/');
        // console.log(pathArray);
        console.log(folderName);
        console.log(`pathString:`, pathString);
        console.log(`current_folder.isOpen:`,current_folder.isOpen)
        let depth = current_folder.depth
        console.log(`current_folder.depth:`,current_folder.depth)

        if (!current_folder.isOpen) {
            let selectedValue = false
            if(current_folder.isSelected){
                selectedValue = true
            }

          console.log(`fetching for: ${pathString}`)
          axios.get('http://localhost:5001/listFiles', {params: { pathString: pathString }})
            .then((response) => {
                current_folder.children = response.data.files.reduce((acc, file) => {
                    const regex = /(\d{4}-\d{2}-\d{2}-\d{4})/;
                    const match = regex.exec(file);
                    const dateTimeString = match && match[1];
                    let configs_path = ""
                    if (current_folder.depth === 3) {
                        const shortenedString = pathString.substring(0, pathString.length - "/videos".length);
                        configs_path = shortenedString+"/configs"
                        console.log(`configs_path: ${configs_path} ${pathString}`)
                    }
                  acc[file] = {
                      isDirectory: true,
                      path: pathString + "/" + file,
                      config_path: configs_path + "/" + dateTimeString + ".yaml",
                      configs_path: configs_path,
                      isOpen: false,
                      isSelected: selectedValue,
                      children: null,
                      depth: depth+1
                  };
                  return acc;
                  }, {})
                current_folder.isOpen = true

                // just react things :)
                const updatedDirectoryStructure = { ...directoryStructure };
                setDirectoryStructure(updatedDirectoryStructure);
              // console.log('Subfolder Contents:', JSON.stringify(newSubfolderContents, null, 2));
              // console.log('directory struct:', JSON.stringify(directoryStructure, null, 2));
            })
            .catch((error) => {
              console.error('Error:', error);
            });
        // }
      }
        else
            {
                current_folder.isOpen = false
                const updatedDirectoryStructure = { ...directoryStructure };
                // just react things :)
                setDirectoryStructure(updatedDirectoryStructure);
                console.log(`current_folder.isOpen:`,current_folder.isOpen)
            }
    };

  useEffect(() => {
  console.log(`useEffect called...`);
    axios.get('http://localhost:5001/listFiles/')
      .then((response) => {
          console.log(`first call response: ${response.data.files}`)
          setDirectoryStructure({ '': { isDirectory: true, path: "", config_path: "", configs_path: "", isOpen: false, isSelected: false, children: null, depth: 0 } });
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  }, []);

  return (
    <div>
        {/* Add or remove the overlay based on the modal state */}
      {/*{isModalOpen && <div className="overlay"></div>}*/}
      <FileTree directoryStructure={directoryStructure} fetchSubfolderContents={fetchSubfolderContents} handleCheck={handleCheck} change_config_path={change_config_path}
      fetchAllSubfolderContents={fetchAllSubfolderContents}/>
      <Button onClick={() => makeJson(directoryStructure)}>make json</Button>
      {isModalOpen && (
        <FileExplorerModal possibleConfigs={possibleConfigs} openModal={openModal} closeModal={closeModal}/>
      )}

    </div>

  );
}

export default VTComponent;
