import React, { useState } from 'react';
// import { Modal, Button, ListGroup } from 'react-bootstrap';
import { useDisclosure } from '@mantine/hooks';
import { Modal, Button, Group } from '@mantine/core';
import './ModalStyles.css';

const FileExplorerModal = ({ openModal, closeModal, possibleConfigs}) => {
  // Add your file list data or fetch it from an API
  const fileList = ['File 1', 'File 2', 'File 3'];

  const renderFiles = (files) => {
      return Object.keys(files).map((file) => (
                      <div key={file}>
                        <span>{file}</span>
                      </div>
                  ));
  }

  return (
              <div className="modal-overlay">
          <div className="modal">
            <div className="modal-content">
                <span className="close" onClick={closeModal}>&times;</span>
                <p>Your modal content goes here!</p>
                {renderFiles(possibleConfigs)}
            </div>
          </div>
        </div>
  );
};

export default FileExplorerModal;
