import React, { useState } from 'react';
import axios from 'axios';

const ServerCommunication = () => {
  const [serverResponse, setServerResponse] = useState('');

  const sendRequest = async () => {
    try {
      const response = await axios.get('http://localhost:5001/listFiles');
      setServerResponse(response.data);
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <div>
      <button onClick={sendRequest}>Send Request to Local Server</button>
      {serverResponse && <div>Server Response: {serverResponse["files"]}</div>}
    </div>
  );
};

export default ServerCommunication;

